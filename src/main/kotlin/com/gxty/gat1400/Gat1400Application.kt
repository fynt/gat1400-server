package com.gxty.gat1400

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Gat1400Application

fun main(args: Array<String>) {
    runApplication<Gat1400Application>(*args)
}
