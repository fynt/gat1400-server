package com.gxty.gat1400.constants

import java.text.DateFormat
import java.text.SimpleDateFormat

object DateTimeConstant {
    /**
     * 时间格式: yyyy-MM-dd HH:mm:ss
     */
    val YMDHMS_NORMAL: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

    /**
     * 时间格式: yyyyMMddHHmmss
     */
    val YMDHMS_TERSE: DateFormat = SimpleDateFormat("yyyyMMddHHmmss")
}