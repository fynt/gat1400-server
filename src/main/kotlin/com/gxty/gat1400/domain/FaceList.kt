package com.gxty.gat1400.domain

data class FaceList(
    val FaceListObject: FaceListObject
)

data class FaceListObject(
    val FaceObject: List<Face>
)

data class Face(
    // 人脸标识
    val FaceID: String,
    // 信息分类 0:其他 1:自动采集 2:人工采集
    val InfoKind: Int,
    // 来源标识
    val SourceID: String,
    // 设备编号
    val DeviceID: String,
    val LeftTopX: Int,
    val LeftTopY: Int,
    val RightBtmX: Int,
    val RightBtmY: Int,
    // 是否驾驶员 0:否 1:是 2:不确定
    val IsDriver: Int,
    // 是否涉外人员 0:否 1:是 2:不确定
    val IsForeigner: Int,
    // 是否涉恐人员 0:否 1:是 2:不确定
    val IsSuspectedTerrorist: Int,
    // 是否涉案人员 0:否 1:是 2:不确定
    val IsCriminalInvolved: Int,
    // 是否在押人员 0:否 1:是 2:不确定
    val IsDetainees: Int,
    // 是否被害人员 0:否 1:是 2:不确定
    val IsVictim: Int,
    // 是否可疑人员 0:否 1:是 2:不确定
    val IsSuspiciousPerson: Int,
    val SubImageList: SubImageList,
)
