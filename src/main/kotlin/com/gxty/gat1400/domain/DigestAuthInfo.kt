package com.gxty.gat1400.domain

data class DigestAuthInfo(
    var username: String? = null,
    var realm: String? = null,
    var nonce: String? = null,
    var uri: String? = null,
    var response: String? = null,
    var qop: String? = null,
    var nc: String? = null,
    var cnonce: String? = null,
)
