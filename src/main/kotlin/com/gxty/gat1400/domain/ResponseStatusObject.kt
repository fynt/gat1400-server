package com.gxty.gat1400.domain

data class ResponseStatusObject(
    val Id: String,
    val LocalTime: String,
    val RequestURL: String,
    val StatusCode: Int,
    val StatusString: String,
)
