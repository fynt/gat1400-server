package com.gxty.gat1400.domain

data class MotorVehicleList(
    val MotorVehicleListObject: MotorVehicleListObject
)

data class MotorVehicleListObject(
    val MotorVehicleObject: List<MotorVehicle>
)

/**
 * 机动车对象特征属性(未完整记录 GAT1400 定义的特征属性字段)
 */
data class MotorVehicle(
    // 车辆标识(车辆全局唯一标识)
    val MotorVehicleID: String,
    // 视频图像信息类型: 0 其他 1 自动采集 2 人工采集
    val InfoKind: Int,
    // 来源标识
    val SourceID: String,
    // 设备编码
    val DeviceID: String,
    // 近景照片
    val StorageUrl1: String,
    // 车牌照片
    val StorageUrl2: String,
    // 远景照片
    val StorageUrl3: String,
    val LeftTopX: Int,
    val LeftTopY: Int,
    val RightBtmX: Int,
    val RightBtmY: Int,
    // 位置标记时间
    val MarkTime: String,
    // 车辆出现时间
    val AppearTime: String,
    // 车辆消失时间
    val DisappearTime: String,
    // 车道号
    val LaneNo: String,
    // 有无车牌
    val HasPlate: Boolean,
    // 号牌种类
    val PlateClass: String,
    // 车牌颜色
    val PlateColor: String,
    // 车牌号
    val PlateNo: String,
    // 行驶速度(km/h)
    val Speed: String,
    // 车辆类型
    val VehicleClass: String,
    // 车辆品牌
    val VehicleBrand: String,
    // 车身颜色
    val VehicleColor: String,
    // 经过时间(过车时间)
    val PassTime: String,
    // 图像列表(可以包含0个或者多个子图像对象)
    val SubImageList: SubImageList,
)

data class SubImageList(
    val SubImageInfoObject: List<SubImageInfo>
)

data class SubImageInfo(
    // 图片ID
    val ImageID: String,
    /**
     * 事件类型
     *
     * 0 其他
     *
     * 1 卡口-过车
     *
     * 2 卡口-过人
     *
     * 3 卡口-打架
     *
     * 4 卡口-快速奔跑
     *
     * 5 目标检测与特征提取-运动目标检测
     *
     * 6 目标检测与特征提取-目标分类
     *
     * 7 目标检测与特征提取-目标颜色检测
     *
     * 8 目标检测与特征提取-行人检测
     *
     * 9 目标检测与特征提取-人员属性分析
     *
     * 10 目标检测与特征提取-人脸检测
     *
     * 11 目标检测与特征提取-人脸比对
     *
     * 12 目标检测与特征提取-车辆检测
     *
     * 13 目标检测与特征提取-车辆比对
     *
     * 14 目标数量分析-流量统计
     *
     * 15 目标数量分析-密度检测
     *
     * 16 目标识别-车辆识别
     *
     * 17 目标识别-车辆基本特征识别
     *
     * 18 目标识别-车辆个体特征识别
     *
     * 19 行为分析-绊线检测
     *
     * 20 行为分析-入侵检测
     *
     * 21 行为分析-逆行检测
     *
     * 22 行为分析-徘徊检测
     *
     * 23 行为分析-遗留物检测
     *
     * 24 行为分析-目标移除检测
     *
     * 25 视频摘要-视频摘要
     *
     * 26 视频增强与复原-去雾
     *
     * 27 视频增强与复原-去模糊
     *
     * 28 视频增强与复原-对比度增强
     *
     * 29 视频增强与复原-低照度视频图像增强
     *
     * 30 视频增强与复原-偏色校正
     *
     * 31 视频增强与复原-宽动态增强
     *
     * 32 视频增强与复原-超分辨率重建
     *
     * 33 视频增强与复原-几何畸变校正
     *
     * 34 视频增强与复原-奇偶场校正
     *
     * 35 视频增强与复原-颜色空间分量分离
     *
     * 36 视频增强与复原-去噪声
     */
    val EventSort: Int,
    // 设备编码
    val DeviceID: String,
    // 储存路径
    val StoragePath: String,
    /**
     * 图像类型
     *
     * 01 车辆大图
     *
     * 02 车牌彩色小图
     *
     * 03 车牌二值化图
     *
     * 04 驾驶员面部特征图
     *
     * 05 副驾驶面部特征图
     *
     * 06 车标
     *
     * 07 违章合成图
     *
     * 08 过车合成图
     *
     * 09 车辆特写图
     *
     * 10 人员图
     *
     * 11 人脸图
     *
     * 12 非机动车图
     *
     * 13 物品图
     *
     * 14 场景图
     *
     * 15 车辆特征图，二进制特征图
     *
     * 16 人员特征图
     *
     * 17 人脸特征图
     *
     * 18 非机动车特征图
     *
     * 19 一般特征图
     *
     * 100 一般图片
     */
    val Type: String,
    // 图像格式
    val FileFormat: String,
    // 拍摄时间(yyyyMMddHHmmss)
    val ShotTime: String,
    // 水平像素值
    val Width: Int,
    // 垂直像素值
    val Height: Int,
    // 图片(base64)
    val Data: String,
)