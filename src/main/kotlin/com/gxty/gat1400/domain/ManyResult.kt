package com.gxty.gat1400.domain

data class ManyResult(
    val ResponseStatusListObject: ResponseStatusList
)

data class ResponseStatusList(
    val ResponseStatusObject: List<ResponseStatusObject>
)