package com.gxty.gat1400.domain

data class SyncTime(
    val SystemTimeObject: SystemTimeObject
)

data class SystemTimeObject(
    val VIIDServerID: String,
    /**
     * 校时模式: 1 网络校时 2 手动校时
     */
    val TimeMode: String,
    val LocalTime: String,
    val TimeZone: String,
)
