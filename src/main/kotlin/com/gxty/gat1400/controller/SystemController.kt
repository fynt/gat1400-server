package com.gxty.gat1400.controller

import com.alibaba.fastjson2.JSONObject
import com.alibaba.fastjson2.toJSONString
import com.gxty.gat1400.annotation.RequireAuth
import com.gxty.gat1400.annotation.Slf4j
import com.gxty.gat1400.annotation.Slf4j.Companion.log
import com.gxty.gat1400.constants.DateTimeConstant.YMDHMS_TERSE
import com.gxty.gat1400.domain.OneResult
import com.gxty.gat1400.domain.ResponseStatusObject
import com.gxty.gat1400.domain.SyncTime
import com.gxty.gat1400.domain.SystemTimeObject
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@Slf4j
@RestController
@RequestMapping("VIID/System")
class SystemController {

    @RequireAuth
    @PostMapping("Register")
    fun register(@RequestBody register: JSONObject): Any {
        log.info("gat1400 register: ${register.toJSONString()}")
        val deviceId = register.getJSONObject("RegisterObject").getString("DeviceID")
        log.info("deviceId : $deviceId 注册成功")
        return ResponseEntity.ok(
            OneResult(
                ResponseStatusObject(deviceId, YMDHMS_TERSE.format(Date()), "/VIID/System/Register", 0, "ok")
            )
        )
    }

    @RequireAuth
    @PostMapping("UnRegister")
    fun unregister(@RequestBody unregister: JSONObject): Any {
        log.info("gat1400 unregister: {}", unregister.toJSONString())
        val deviceId = unregister.getJSONObject("UnRegisterObject").getString("DeviceID")
        log.info("deviceId : $deviceId 注销成功")
        return ResponseEntity.ok(
            OneResult(
                ResponseStatusObject(deviceId, YMDHMS_TERSE.format(Date()), "/VIID/System/UnRegister", 0, "ok")
            )
        )
    }

    @RequireAuth
    @PostMapping("Keepalive")
    fun keepalive(@RequestBody keepalive: JSONObject): Any {
        log.info("gat1400 keepalive: {}", keepalive.toJSONString())
        val deviceId = keepalive.getJSONObject("KeepaliveObject").getString("DeviceID")
        log.info("deviceId : $deviceId 上报心跳")
        return ResponseEntity.ok(
            OneResult(
                ResponseStatusObject(deviceId, YMDHMS_TERSE.format(Date()), "/VIID/System/Keepalive", 0, "ok")
            )
        )
    }

    @RequireAuth
    @GetMapping("Time")
    fun time(): Any {
        log.info("gat1400 time: 校时,无请求体")
        val syncTime = SyncTime(
            // TimeMode 校时模式: 1 网络校时 2 手动校时
            SystemTimeObject("12345", "1", YMDHMS_TERSE.format(Date()), "Asia/Shanghai")
        )
        log.info("syncTime: ${syncTime.toJSONString()}")
        return ResponseEntity.ok(syncTime)
    }
}

