package com.gxty.gat1400.controller

import com.gxty.gat1400.annotation.Slf4j
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Slf4j
@RestController
@RequestMapping("VIID/NonMotorVehicles")
class NonMotorVehiclesController {
}