package com.gxty.gat1400.controller

import com.alibaba.fastjson2.JSONArray
import com.gxty.gat1400.annotation.RequireAuth
import com.gxty.gat1400.annotation.Slf4j
import com.gxty.gat1400.annotation.Slf4j.Companion.log
import com.gxty.gat1400.constants.DateTimeConstant.YMDHMS_TERSE
import com.gxty.gat1400.domain.ManyResult
import com.gxty.gat1400.domain.ResponseStatusList
import com.gxty.gat1400.domain.ResponseStatusObject
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@Slf4j
@RestController
@RequestMapping("VIID/Persons")
class PersonsController {

    @RequireAuth
    @PostMapping
    fun persons(@RequestBody personList: JSONArray): Any {
        log.info("gat1400 persons: ${personList.toJSONString()}")
        return ResponseEntity.ok(
            ManyResult(
                ResponseStatusList(
                    listOf(
                        ResponseStatusObject(
                            "",
                            YMDHMS_TERSE.format(Date()),
                            "/VIID/Persons",
                            0,
                            "ok"
                        )
                    )
                )
            )
        )
    }
}