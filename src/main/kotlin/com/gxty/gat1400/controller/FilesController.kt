package com.gxty.gat1400.controller

import com.alibaba.fastjson2.JSONArray
import com.gxty.gat1400.annotation.RequireAuth
import com.gxty.gat1400.annotation.Slf4j
import com.gxty.gat1400.annotation.Slf4j.Companion.log
import com.gxty.gat1400.constants.DateTimeConstant.YMDHMS_TERSE
import com.gxty.gat1400.domain.ManyResult
import com.gxty.gat1400.domain.OneResult
import com.gxty.gat1400.domain.ResponseStatusList
import com.gxty.gat1400.domain.ResponseStatusObject
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@Slf4j
@RestController
@RequestMapping("VIID/Files")
class FilesController {

    @RequireAuth
    @PostMapping
    fun files(@RequestBody fileList: JSONArray): Any {
        log.info("gat1400 files: ${fileList.toJSONString()}")
        return ResponseEntity.ok(
            ManyResult(
                ResponseStatusList(
                    listOf(
                        ResponseStatusObject(
                            "",
                            YMDHMS_TERSE.format(Date()),
                            "/VIID/Files",
                            0,
                            "ok"
                        )
                    )
                )
            )
        )
    }

    @RequireAuth
    @PostMapping("{ID}/Data")
    fun fileData(@PathVariable ID: String, data: String): Any {
        log.info("gat1400 fileData ID: $ID data: ${data.slice(0..14)}")
        return ResponseEntity.ok(
            OneResult(
                ResponseStatusObject(
                    "",
                    YMDHMS_TERSE.format(Date()),
                    "/VIID/Files/$ID/Data",
                    0,
                    "ok"
                )
            )
        )
    }
}