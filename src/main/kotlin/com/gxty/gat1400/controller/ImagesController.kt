package com.gxty.gat1400.controller

import com.alibaba.fastjson2.JSONArray
import com.gxty.gat1400.annotation.RequireAuth
import com.gxty.gat1400.annotation.Slf4j
import com.gxty.gat1400.annotation.Slf4j.Companion.log
import com.gxty.gat1400.constants.DateTimeConstant
import com.gxty.gat1400.domain.ManyResult
import com.gxty.gat1400.domain.OneResult
import com.gxty.gat1400.domain.ResponseStatusList
import com.gxty.gat1400.domain.ResponseStatusObject
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@Slf4j
@RestController
@RequestMapping("VIID/Images")
class ImagesController {

    @RequireAuth
    @PostMapping
    fun images(@RequestBody imageList: JSONArray): Any {
        log.info("gat1400 images: ${imageList.toJSONString()}")
        return ResponseEntity.ok(
            ManyResult(
                ResponseStatusList(
                    listOf(
                        ResponseStatusObject(
                            "",
                            DateTimeConstant.YMDHMS_TERSE.format(Date()),
                            "/VIID/Images",
                            0,
                            "ok"
                        )
                    )
                )
            )
        )
    }

    @RequireAuth
    @PostMapping("{ID}/Data")
    fun imageData(@PathVariable ID: String, data: String): Any {
        log.info("gat1400 imageData ID: $ID data: ${data.slice(0..14)}")
        return ResponseEntity.ok(
            OneResult(
                ResponseStatusObject(
                    "",
                    DateTimeConstant.YMDHMS_TERSE.format(Date()),
                    "/VIID/VideoSlices/$ID/Data",
                    0,
                    "ok"
                )
            )
        )
    }
}